module "vnet_terraform" {
  vnet_name           = "vnet-test-terraform"
  source              = "Azure/vnet/azurerm"
  resource_group_name = azurerm_resource_group.vm.name
  address_space       = ["10.150.0.0/16"]
  subnet_prefixes     = ["10.150.0.0/17", "10.150.128.0/17"]
  subnet_names        = ["snet-test-terraform-1", "snet-test-terraform-2"]

  nsg_ids = {
    snet-test-terraform-1 = module.squid_nsg.network_security_group_id
    snet-test-terraform-2 = module.squid_nsg.network_security_group_id
  }

  tags = {
    source = "terraform"
  }
  depends_on = [azurerm_resource_group.vm]
}
