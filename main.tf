module "example" {
  # Name of the VM. Suffix will automatically be added based on number of instances. Starting from -0. e.g proxy-0
  vm_name = "example"

  # Where the module is located.
  source = "./squid_proxy"

  # Name of the resource group created in the resource_groups.tf file. 
  resource_group_name = azurerm_resource_group.vm.name

  # OS name to deploy. See ./squid_proxy/variables.tf for a list of values.
  vm_os_simple = "UbuntuServer"

  # Id of the virtual network subnet created in the virtual_networks.tf file
  vnet_subnet_id = module.vnet_terraform.vnet_subnets[0]

  # SSH public key to use for passwordless authentication
  ssh_key = "./ssh/id_rsa.pub"

  # VM administrator username
  admin_username = "azureuser"

  # Tell Terraform to create the resource group before running the module
  depends_on = [azurerm_resource_group.vm]

  # Squid proxy username and password to configure
  squid_username = "test"
  squid_password = "test"
  squid_port     = 3128

  # Number of instances to start
  nb_instances = 1
}
