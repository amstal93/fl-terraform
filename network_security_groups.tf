module "squid_nsg" {
  source              = "Azure/network-security-group/azurerm"
  resource_group_name = azurerm_resource_group.vm.name
  security_group_name = "nsg-terraform-squid"
  predefined_rules = [
    {
      name     = "SSH"
      priority = "500"
    }
  ]
  custom_rules = [
    {
      name                   = "squid-proxy"
      priority               = "200"
      direction              = "Inbound"
      access                 = "Allow"
      protocol               = "tcp"
      destination_port_range = "3128"
      description            = "squid-proxy"
    }
  ]
  tags = {
    source = "terraform"
  }
  depends_on = [azurerm_resource_group.vm]
}
