## Basic setup
1. Install and verify Terraform on your system following this [guide](https://learn.hashicorp.com/tutorials/terraform/install-cli) from Terraform.
2. Clone this repo to a folder on your computer with `git clone https://gitlab.com/regg00/fl-terraform.git`
3. Change working directory for the one created by git `cd fl-terraform`
4. Initialize Terraform with `terraform init`. This will download the required modules and providers.
5. If not already done, install the Azure CLI tool and run `az login`. Set the proper subscription id with `az account set --subscription="SUBSCRIPTION_ID"`. You can see a list of every subscriptions with `az account list`.
6. Create and save a Terraform plan with `terraform plan -out planfile`
7. Validate the output. If everything is in order, deploy the plan with `terraform apply planfile` and wait for the command to finish.


## How to add a VM
Open the *main.tf* file in a text editor. A VM definition look something like this:
```powershell
module "example" {
  # Name of the VM. Suffix will automatically be added based on number of instances. Starting from -0. e.g proxy-0
  vm_name = "example"

  # Where the squid_proxy module is located.
  source = "./squid_proxy"

  # Name of the resource group created in the resource_groups.tf file. 
  resource_group_name = azurerm_resource_group.vm.name

  # OS name to deploy. See ./squid_proxy/variables.tf for a list of values.
  vm_os_simple = "UbuntuServer"

  # Id of the virtual network subnet created in the virtual_networks.tf file
  vnet_subnet_id = module.vnet_terraform.vnet_subnets[0]

  # SSH public key to use for passwordless authentication
  ssh_key = "./ssh/id_rsa.pub"

  # VM administrator username
  admin_username = "azureuser"

  # Tell Terraform to create the resource group before running the module
  depends_on = [azurerm_resource_group.vm]

  # Squid proxy username and password to configure
  squid_username = "test"
  squid_password = "test"

  # Number of instances to start
  nb_instances = 1
}
```

In the example above, a module named **example** was defined. That module used the custom **squid_proxy** module to create and configure a proxy server named **example-<ID>**.

**To create another proxy with the same parameters (username, password, etc.), you can change increase the *nb_instances* value.**

**To create a different proxy, simply copy the module instance and rename the VM and module name.**

After the creation of resources, a JSON file is created in the current directory. That file is named after the module name, followed by *_out* (e.g. **example_out.json**) and contains the basic information of every proxies created for that instance of the module. You can see an example right here:
```json
[
    {
        "name": "example-0",
        "public_ip": "52.232.132.58",
        "squid_password": "test",
        "squid_username": "test"
    }
]
```

## How to list created resources
Use the command `terraform state list` to list every resource created :
```
> terraform state list

azurerm_resource_group.vm
module.example.data.azurerm_public_ip.vm[0]
module.example.data.azurerm_resource_group.vm
module.example.data.template_file.squid_cloud_init
module.example.azurerm_network_interface.vm[0]
module.example.azurerm_public_ip.vm[0]
module.example.azurerm_virtual_machine.vm[0]
module.example.local_file.created_resources
module.example.random_string.random_password
module.example.random_string.random_user
module.squid_nsg.data.azurerm_resource_group.nsg
module.squid_nsg.azurerm_network_security_group.nsg
module.squid_nsg.azurerm_network_security_rule.custom_rules[0]
module.squid_nsg.azurerm_network_security_rule.predefined_rules[0]
module.vnet_terraform.data.azurerm_resource_group.vnet
module.vnet_terraform.azurerm_subnet.subnet[0]
module.vnet_terraform.azurerm_subnet.subnet[1]
module.vnet_terraform.azurerm_subnet_network_security_group_association.vnet["snet-test-terraform-1"]
module.vnet_terraform.azurerm_subnet_network_security_group_association.vnet["snet-test-terraform-2"]
module.vnet_terraform.azurerm_virtual_network.vnet
```

You can also list resources created by a module instance:
```
> terraform state list module.example

module.example.data.azurerm_public_ip.vm[0]
module.example.data.azurerm_resource_group.vm
module.example.data.template_file.squid_cloud_init
module.example.azurerm_network_interface.vm[0]
module.example.azurerm_public_ip.vm[0]
module.example.azurerm_virtual_machine.vm[0]
module.example.local_file.created_resources
module.example.random_string.random_password
module.example.random_string.random_user
```

## How to destroy resources
To delete every resources created by terraform, use `terraform destroy`. You will get a list of everything that will be deleted and be asked to confirm. See below for an example. Output was truncated for clarity.
```
> terraform destroy

...

Terraform will perform the following actions:

  # azurerm_resource_group.vm will be destroyed
  - resource "azurerm_resource_group" "vm" {
      - id       = "/subscriptions/eb40f514-1316-4dfc-827b-9abb8faecc19/resourceGroups/rg-example-terraform-1" -> null
      - location = "canadaeast" -> null
      - name     = "rg-example-terraform-1" -> null
      - tags     = {} -> null
    }

  # module.example.azurerm_network_interface.vm[0] will be destroyed
  - resource "azurerm_network_interface" "vm" {
      - applied_dns_servers           = [] -> null
      - dns_servers                   = [] -> null
      - enable_accelerated_networking = false -> null
      - enable_ip_forwarding          = false -> null
      - id                            = "/subscriptions/eb40f514-1316-4dfc-827b-9abb8faecc19/resourceGroups/rg-example-terraform-1/providers/Microsoft.Network/networkInterfaces/example-nic-0" -> null
      - internal_domain_name_suffix   = "zs1gumqv1oguffkfezhcmhgvnf.vx.internal.cloudapp.net" -> null
      - location                      = "canadaeast" -> null
      - mac_address                   = "00-0D-3A-F4-49-E2" -> null
      - name                          = "example-nic-0" -> null
      - private_ip_address            = "10.150.0.4" -> null
      - private_ip_addresses          = [
          - "10.150.0.4",
        ] -> null
      - resource_group_name           = "rg-example-terraform-1" -> null
      - tags                          = {
          - "source" = "terraform"
        } -> null
      - virtual_machine_id            = "/subscriptions/eb40f514-1316-4dfc-827b-9abb8faecc19/resourceGroups/rg-example-terraform-1/providers/Microsoft.Compute/virtualMachines/example-0" -> null

      - ip_configuration {
          - name                          = "example-ip-0" -> null
          - primary                       = true -> null
          - private_ip_address            = "10.150.0.4" -> null
          - private_ip_address_allocation = "Dynamic" -> null
          - private_ip_address_version    = "IPv4" -> null
          - public_ip_address_id          = "/subscriptions/eb40f514-1316-4dfc-827b-9abb8faecc19/resourceGroups/rg-example-terraform-1/providers/Microsoft.Network/publicIPAddresses/example-pip-0" -> null
          - subnet_id                     = "/subscriptions/eb40f514-1316-4dfc-827b-9abb8faecc19/resourceGroups/rg-example-terraform-1/providers/Microsoft.Network/virtualNetworks/vnet-test-terraform/subnets/snet-test-terraform-1" -> null
        }
    }

...


Plan: 0 to add, 0 to change, 15 to destroy.

Changes to Outputs:
  - example_created_proxies = [
      - {
          - name           = "example-0"
          - public_ip      = "52.232.132.58"
          - squid_password = "test"
          - squid_username = "test"
        },
    ] -> null


...

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value:
```

You can also target a specific module with destroy: `terraform destroy -target module.example`


## Available parameters for the squid_proxy module
The following parameters can be defined when calling the squid_proxy module. This list can be outdated. Refer to the **variables.tf** file inside the squid_proxy folder for the exact information.
```javascript
variable "vm_name" {
  type        = string
  description = "local name of the Virtual Machine."
}

variable "resource_group_name" {
  type        = string
  description = ""
}

variable "vm_os_simple" {
  type        = string
  description = "Specify UbuntuServer, WindowsServer, RHEL, openSUSE-Leap, CentOS, Debian, CoreOS and SLES to get the latest image version of the specified os."
  default     = "UbuntuServer"
}

variable "vnet_subnet_id" {
  type        = string
  default     = ""
  description = "The subnet id of the virtual network where the virtual machines will reside."
}

variable "vm_size" {
  description = "Specifies the size of the virtual machine."
  type        = string
  default     = "Standard_B1s"
}

variable "tags" {
  type        = map(string)
  description = "A map of the tags to use on the resources that are deployed with this module."

  default = {
    source = "terraform"
  }
}

variable "ssh_key" {
  description = "Path to the public key to be used for ssh access to the VM.  Only used with non-Windows vms and can be left as-is even if using Windows vms. If specifying a path to a certification on a Windows machine to provision a linux vm use the / in the path versus backslash. e.g. c:/home/id_rsa.pub."
  type        = string
  default     = "~/.ssh/id_rsa.pub"
}

variable "admin_username" {
  description = "The admin username of the VM that will be deployed."
  type        = string
  default     = "azureuser"
}

variable "nb_instances" {
  description = "Specify the number of vm instances."
  type        = number
  default     = 1
}

variable "location" {
  description = "(Optional) The location in which the resources will be created."
  type        = string
  default     = ""
}

variable "delete_os_disk_on_termination" {
  type        = bool
  description = "Delete datadisk when machine is terminated."
  default     = true
}
variable "storage_account_type" {
  description = "Defines the type of storage account to be created. Valid options are Standard_LRS, Standard_ZRS, Standard_GRS, Standard_RAGRS, Premium_LRS."
  type        = string
  default     = "Premium_LRS"
}
variable "vm_os_id" {
  description = "The resource ID of the image that you want to deploy if you are using a custom image."
  type        = string
  default     = ""
}
variable "vm_os_publisher" {
  description = "The name of the publisher of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  type        = string
  default     = ""
}

variable "vm_os_offer" {
  description = "The name of the offer of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  type        = string
  default     = ""
}

variable "vm_os_sku" {
  description = "The sku of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  type        = string
  default     = ""
}

variable "vm_os_version" {
  description = "The version of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  type        = string
  default     = "latest"
}
variable "nb_data_disk" {
  description = "(Optional) Number of the data disks attached to each virtual machine."
  type        = number
  default     = 0
}

variable "enable_ssh_key" {
  type        = bool
  description = "(Optional) Enable ssh key authentication in Linux virtual Machine."
  default     = true
}

variable "allocation_method" {
  description = "Defines how an IP address is assigned. Options are Static or Dynamic."
  type        = string
  default     = "Dynamic"
}
variable "nb_public_ip" {
  description = "Number of public IPs to assign corresponding to one IP per vm. Set to 0 to not assign any public IP addresses."
  type        = number
  default     = 1
}
variable "admin_password" {
  description = "The admin password to be used on the VMSS that will be deployed. The password must meet the complexity requirements of Azure."
  type        = string
  default     = ""
}

variable "squid_username" {
  description = "Squid Proxy username"
  type        = string
  default     = "testUser"
}

variable "squid_password" {
  description = "Squid Proxy password"
  type        = string
  default     = "testPassword"
}

variable "randomize_user" {
  description = "Should the username be randomized"
  default     = false
}

variable "randomize_password" {
  description = "Should the password be randomized"
  default     = false
}
```