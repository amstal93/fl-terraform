resource "azurerm_resource_group" "vm" {
  # Name of the resource groupe to create
  name = "rg-example-terraform-1"

  # Location/region where to put everythin. See ./squid_proxy/variables.tf for a list of values.
  location = var.locations[var.location]
}
