#! /bin/bash
# Install and configure squid-proxy
wget https://raw.githubusercontent.com/serverok/squid-proxy-installer/master/squid3-install.sh
chmod 755 squid3-install.sh
sudo ./squid3-install.sh

# Create a user with specified username and password
sudo htpasswd -b -c /etc/squid/passwd ${proxy_username} ${proxy_password}
sudo service squid restart