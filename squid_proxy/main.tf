module "os" {
  source       = "./os"
  vm_os_simple = var.vm_os_simple
}

data "azurerm_resource_group" "vm" {
  name = var.resource_group_name
}


resource "azurerm_virtual_machine" "vm" {
  count               = var.nb_instances
  name                = "${var.vm_name}-${count.index}"
  resource_group_name = data.azurerm_resource_group.vm.name
  location            = coalesce(var.location, data.azurerm_resource_group.vm.location)
  vm_size             = var.vm_size

  network_interface_ids = [element(azurerm_network_interface.vm.*.id, count.index)]

  delete_os_disk_on_termination = var.delete_os_disk_on_termination


  storage_image_reference {
    id        = var.vm_os_id
    publisher = var.vm_os_id == "" ? coalesce(var.vm_os_publisher, module.os.calculated_value_os_publisher) : ""
    offer     = var.vm_os_id == "" ? coalesce(var.vm_os_offer, module.os.calculated_value_os_offer) : ""
    sku       = var.vm_os_id == "" ? coalesce(var.vm_os_sku, module.os.calculated_value_os_sku) : ""
    version   = var.vm_os_id == "" ? var.vm_os_version : ""
  }

  storage_os_disk {
    name              = "osdisk-${var.vm_name}-${count.index}"
    create_option     = "FromImage"
    caching           = "ReadWrite"
    managed_disk_type = var.storage_account_type
  }

  dynamic storage_data_disk {
    for_each = range(var.nb_data_disk)
    content {
      name              = "${var.vm_name}-datadisk-${count.index}-${storage_data_disk.value}"
      create_option     = "Empty"
      lun               = storage_data_disk.value
      disk_size_gb      = var.data_disk_size_gb
      managed_disk_type = var.data_sa_type
    }
  }

  os_profile {
    computer_name  = "${var.vm_name}-${count.index}"
    admin_username = var.admin_username
    admin_password = var.admin_password
    custom_data    = base64encode(data.template_file.squid_cloud_init.rendered)
  }

  os_profile_linux_config {
    disable_password_authentication = var.enable_ssh_key

    dynamic ssh_keys {
      for_each = var.enable_ssh_key ? [var.ssh_key] : []
      content {
        path     = "/home/${var.admin_username}/.ssh/authorized_keys"
        key_data = file(var.ssh_key)
      }
    }
  }

  tags = var.tags

}

resource "azurerm_network_interface" "vm" {
  count               = var.nb_instances
  name                = "${var.vm_name}-nic-${count.index}"
  resource_group_name = data.azurerm_resource_group.vm.name
  location            = coalesce(var.location, data.azurerm_resource_group.vm.location)

  ip_configuration {
    name                          = "${var.vm_name}-ip-${count.index}"
    subnet_id                     = var.vnet_subnet_id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = length(azurerm_public_ip.vm.*.id) > 0 ? element(concat(azurerm_public_ip.vm.*.id, list("")), count.index) : ""
  }

  tags = var.tags
}
resource "azurerm_public_ip" "vm" {
  count               = var.nb_instances
  name                = "${var.vm_name}-pip-${count.index}"
  resource_group_name = data.azurerm_resource_group.vm.name
  location            = coalesce(var.location, data.azurerm_resource_group.vm.location)
  allocation_method   = var.allocation_method
  tags                = var.tags
}

data "template_file" "squid_cloud_init" {
  template = file("${path.module}/scripts/squid_installer.sh")
  vars = {
    proxy_username = var.randomize_user ? random_string.random_user.result : var.squid_username
    proxy_password = var.randomize_password ? random_string.random_password.result : var.squid_password
  }
}

data "azurerm_public_ip" "vm" {
  count               = var.nb_instances
  name                = azurerm_public_ip.vm[count.index].name
  resource_group_name = data.azurerm_resource_group.vm.name
  depends_on          = [azurerm_virtual_machine.vm]
}

resource "random_string" "random_user" {
  length  = 7
  special = false
}
resource "random_string" "random_password" {
  length  = 7
  special = false
}
